package storages

import (
	"gitlab.com/mezigar/goboilerplate/internal/db/adapter"
	"gitlab.com/mezigar/goboilerplate/internal/infrastructure/cache"
	vstorage "gitlab.com/mezigar/goboilerplate/internal/modules/auth/storage"
	ustorage "gitlab.com/mezigar/goboilerplate/internal/modules/user/storage"
)

type Storages struct {
	User   ustorage.Userer
	Verify vstorage.Verifier
}

func NewStorages(sqlAdapter *adapter.SQLAdapter, cache cache.Cache) *Storages {
	return &Storages{
		User:   ustorage.NewUserStorage(sqlAdapter, cache),
		Verify: vstorage.NewEmailVerify(sqlAdapter),
	}
}
